import express from "express";
import bodyParser from "body-parser";
import { RegisterRoutes } from "./routes/routes";
import * as swaggerUi from "swagger-ui-express";
import * as swaggerDocument from "./swagger.json";

export const app = express();

// Use body parser to read sent json payloads
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
RegisterRoutes(app);

app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument, {}));

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
